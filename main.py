# use to parse the command line argument
import argparse
# interaction with the system(run time environment)
import sys
# for connection of sql
import pymysql
# for read csv file
import csv

# connection with mysql
db = pymysql.connect(host="localhost",user="root",password="manager",database="dataeaze")
# we can insert using cursor
cursor = db.cursor()

#Action objects are used by an ArgumentParser to represent the information needed to parse a single argument from
# one or more strings from the command line.
parser = argparse.ArgumentParser('csv')

# Filling an ArgumentParser with information about program arguments is done by making calls to the add_argument() method.
# Generally, these calls tell the ArgumentParser how to take the strings on the command line and turn them into objects.
parser.add_argument('csv')
args = parser.parse_args()

# sys.argv() is an array for command line arguments in Python. To employ this module named “sys” is used.
# sys.argv is similar to an array and the values are also retrieved like Python array.
if len( sys.argv ) > 1:
  with open( args.csv ) as csvFile:
    reader = csv.reader(csvFile, delimiter=',')
    next(reader)
    for line in reader:
      cursor.execute('INSERT INTO Advirtise(TV ,radio ,newspaper ,sales) VALUES(%s, %s, %s, %s)', line)

db.commit()
cursor.close()